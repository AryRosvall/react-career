import React, {Component} from 'react'
import RegularError from '../../error/components/regular-error'

class HandleError extends Component {

  state =  {
    handleError: false,
  }

  componentDidCatch(error, info){
    //envía este error a un servicio como entry
    this.setState({
      handleError:true,
    })
  }
  render(){
    if(this.state.handleError){
      return(<RegularError/>)
    }
    return(this.props.children)
  }
}
export default HandleError;
