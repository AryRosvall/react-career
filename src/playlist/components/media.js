import React, { PureComponent } from 'react';
import './media.css';
import PropTypes from 'prop-types';

class Media extends React.PureComponent {

  handleClick = (event) => {
    this.props.openModal(this.props.id);
  }

  render() {
    return (
      <div className="Media" onClick={this.handleClick}>
        <div className="Media-cover">
          <img className="Media-image" src={this.props.cover}
            alt=""
            width={260}
            height={160} />
        </div>
        <h3 className="Media-title">{this.props.title}</h3>
        <p className="Media-author">{this.props.author}</p>
      </div>
    )
  }
}

//Valida el tipo de dato de los parámetros
Media.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  author: PropTypes.string,
  type: PropTypes.oneOf(['video', 'audio']),
}

//Exporta este componente
export default Media;
