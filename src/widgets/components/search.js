import React from 'react'
import './search.css'

const Search = (props) =>(

//function Search (props){
//console.log(props);
//  return (

  <form
    className="Search"
    onSubmit={props.handleSubmit}
  >
    <input
      ref={props.setInputRef}
      type="text"
      placeholder="Busca tu video favorito"
      className="Search-input"
      name="search"
      onChange={props.handleChange}
      value={props.value}
    />
  </form>
)

export default Search;
