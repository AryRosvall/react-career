import React, { Component } from 'react'
import VideoPlayerLayout from '../components/video-player-layout'
import Video from '../components/video'
import Title from '../components/title'
import PlayPause from '../components/play-pause'
import Timer from '../components/timer'
import Controls from '../components/video-player-controls'
import ProgressBar from '../components/progress-bar'
import Spinner from '../components/spinner'
import Volume from '../components/volume'
import FullScreen from '../components/full-screen'
import { connect } from 'react-redux'
class VideoPlayer extends Component {
  state = {
    pause: true,
    duration: 0,
    currentTime: 0,
    FormattedDuration: '00',
    FormattedcurrentTime: '00',
    loading: false,
    mute: false,
    Volume: 1,
    LastVolume: null
  }

  tooglePlay = event => {
    this.setState({
      pause: !this.state.pause
    })
  }

  componentDidMount() {
    this.setState({
      pause: (!this.props.autoplay)
    })
  }

  leftPad(number) {
    const pad = "00";
    return pad.substring(0, pad.length - number.length) + number;
  }

  formattedTime(secs) {
    const minutes = parseInt(secs / 60, 10);
    const seconds = parseInt(secs % 60, 10);
    return `${this.leftPad(minutes.toString())}:${this.leftPad(seconds.toString())}`
  }

  handleLoadedMetadata = event => {
    this.video = event.target;

    this.setState({
      FormattedDuration: this.formattedTime(this.video.duration),
      duration: this.video.duration
    })
  }

  handleTimeUpdate = event => {
    this.video = event.target;
    this.setState({
      FormattedcurrentTime: this.formattedTime(this.video.currentTime),
      currentTime: this.video.currentTime
    })
  }

  handleProgressChange = event => {
    this.video.currentTime = event.target.value;
  }

  handleSeeking = event => {
    this.setState({
      loading: true
    })
  }

  handleSeeked = event => {
    this.setState({
      loading: false
    })
  }

  handleVolumeChange = event => {
    this.video.volume = event.target.value;
    this.setState({
      Volume: this.video.volume
    })
  }

  handleVolumeClick = event => {
    if (this.video.volume != 0) {
      this.setState({
        LastVolume: this.video.volume
      })
      this.video.volume = 0;
      this.setState({
        Volume: this.video.volume
      })
    } else {
      this.setState({
        Volume: this.state.LastVolume
      })
      this.video.volume = this.state.LastVolume;
    }
  }

  handleFullScreenClick = event => {

    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
      if (!document.mozFullScreen) {
        this.player.mozRequestFullScreen();
      } else {
        document.mozCancelFullScreen()
      }
    } else {
      if (!document.webkitIsFullScreen) {
        this.player.webkitRequestFullscreen();
      } else {
        document.webkitExitFullscreen();
      }
    }
  }

  setRef = element => {
    this.player = element;
  }

  render() {

    return (
      <VideoPlayerLayout
        setRef={this.setRef}>
        <Title title={this.props.media.get('title')} />
        <Controls>
          <PlayPause
            pause={this.state.pause}
            handleClick={this.tooglePlay} />
          <Timer
            duration={this.state.FormattedDuration}
            currentTime={this.state.FormattedcurrentTime} />
          <ProgressBar
            max={this.state.duration}
            value={this.state.currentTime}
            handleProgressChange={this.handleProgressChange}
          />
          <Spinner
            active={this.state.loading}
          />
          <Volume
            handleVolumeChange={this.handleVolumeChange}
            handleVolumeClick={this.handleVolumeClick}
            value={this.state.Volume}
          />
          <FullScreen
            handleFullScreenClick={this.handleFullScreenClick}
          />
        </Controls>
        <Video
          pause={this.state.pause}
          autoPlay={this.props.autoplay}
          handleLoadedMetadata={this.handleLoadedMetadata}
          handleTimeUpdate={this.handleTimeUpdate}
          handleSeeking={this.handleSeeking}
          handleSeeked={this.handleSeeked}
          src={this.props.media.get('src')}
        />
      </VideoPlayerLayout>
    )
  }
}

function mapStateToProps (state, props){
  return {
    media: state.get('data').get('entities').get('media').get(props.id)
  }
}
export default connect(mapStateToProps)(VideoPlayer);
