import React from 'react'
import './video-player.controls.css'

const VideoPlayerControlls = (props) =>(

  <div className="VideoPlayerControls">
    {props.children}
  </div>
)

export default VideoPlayerControlls;
