import React, {Component} from 'react'
import './volume.css'
import VolumeIcon from '../../icons/components/volume'

function Volume (props) {

  return(
    <button className="Volume">
      <div onClick={props.handleVolumeClick}>
        <VolumeIcon size={25}
          color= {props.value==0 ? "red":"white"}
          />
      </div>

      <div className="Volume-range">
        <input
          type="range"
          min={0}
          max={1}
          step={0.05}
          value={props.value}
          onChange={props.handleVolumeChange}
          />
      </div>
    </button>
  )
}

export default Volume;
