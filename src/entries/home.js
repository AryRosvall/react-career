import React from 'react';
import { render } from 'react-dom';
import Home from '../pages/containers/home';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from '../reducers/index';
import { Map as map } from 'immutable';
import logger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

// Middleware forma larga
// function logger({ getState, dispatch }) {
//     // metodo para despachar el siguiente middleware
//     return (next) => {
//         return (action) => {
//             console.log('vamos a enviar esta acción', action)
//             const value = next(action)np
//             console.log('este es mi nuevo estado', getState().toJS())
//             return value
//         }
//     }
// }

//Middleware forma corta
// const logger = ({ getState, dispatch }) => next => action => {
//     console.log('vamos a enviar esta acción', action)
//     const value = next(action)
//     console.log('este es mi nuevo estado', getState().toJS())
//     return value
// }
const store = createStore(
    reducer,
    map(),
    composeWithDevTools(
        applyMiddleware(
            logger,
            thunk),
    )
)

const home = document.getElementById('home-container');

//ReactDOM.render(que voy a renderizar, donde lo voy a hacer);
render(
    <Provider store={store}>
        <Home />
    </Provider>,
    home);
